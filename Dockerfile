FROM golang:1.21.5-alpine AS builder

WORKDIR /src
COPY . .

RUN apk add --no-cache git && \
    go mod download && \
    CGO_ENABLED=0 go build -ldflags="-s -w" -o "SignTools-Builder"

FROM alpine:3.18.4
RUN apk add --no-cache build-base ruby-dev python3 nodejs curl
RUN gem install bundler
RUN gem install fastlane

WORKDIR /

COPY --from=builder /src/SignTools-Builder /
COPY . .

ENTRYPOINT ["/SignTools-Builder", "-key", "thisisapassword", "-files", "SignTools-CI"]
EXPOSE 8090
